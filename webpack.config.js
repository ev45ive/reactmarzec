var path = require('path')
var HTMLWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
    entry: [
        './src/main.js'
    ],
    output:{
        path: path.join(__dirname,'dist'),
        filename: '[name].js'
    },
    devtool: 'sourcemap',
    module:{
        rules:[
            { test:/\.jsx?$/, use: ['babel-loader'] },
            { test:/\.css$/, use: ExtractTextPlugin.extract({ 
                fallback: 'style-loader', 
                use: 'css-loader' }) }
        ]
    },
    plugins:[
        new ExtractTextPlugin('styles.css'),
        new HTMLWebpackPlugin({ template: './src/index.html'})
    ],
    devServer:{
        historyApiFallback: true
    }
    // proxy: {
    //     // ... .NET MVC URL ...
    // }
}