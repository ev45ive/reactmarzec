import React from 'react';
import {store, load_users, select_user} from '../stores/reduxStore.js'

import { Route, Link } from 'react-router-dom'

window.load = load_users

const UsersList = ({users = [],onSelect,selected}) => (
    (!users.length ? <div> Brak wyników </div>  : 
    <ul className="list-group">
    {users.map( user => (
        <li className={"list-group-item "+(selected==user?"active":null)} 
            key={user.id} 
            onClick={()=>onSelect(user)}> {user.name} </li>
    ))}
    </ul>)
)

export class Users extends React.Component{
    constructor(){
        super()
        this.state = {
        }
        this.pending
    }

    componentWillMount(){
        this.setState({
            users: store.getState().users.data,
            selected: store.getState().users.selected
        })
        
        this.unsubscribe = store.subscribe( ()=>{
            let {data:users, pending, error, selected, entities } = store.getState().users;

            selected = entities[selected]


            this.setState({users,pending,error, selected})
        })
    }
    componentWillUnmount(){
        this.unsubscribe()
    }


    select = (user) => {
        select_user(user)
        this.props.history.push('/users/'+user.id)  
  }

    render(){
        const {users, selected, pending, error} = this.state;
        return <div className="row">
            <div className="col">
                <button onClick={load_users}>Load!</button>
                {error ? <div>{error}</div> : (pending? <div> Ładowanie ... </div> :
                <UsersList users={users} selected={selected} onSelect={this.select} />)}
            </div>
            <div className="col">

    {/*{selected && <Link to={'/users/'+selected.id} > {selected.id} </Link>} */}
            
            <Route path="/users/:id" render={( {match:{params:{id}}} )=><div> 
                { id } 
            </div> } />
            <Route path={this.props.match.url} exact render={()=><div>
                Select User
            </div>} />

            </div>
        </div>
    }
}