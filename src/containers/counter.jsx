 import React from 'react'
 import { createStore, combineReducers } from 'redux'

 const INCREMENT = {
     type: 'INCREMENT'
 }
 
 const DECREMENT = {
     type: 'DECREMENT'
 }

const initialState = {
    counter: 0,
    counter2: 0
};

/*const counterReducer = (state, action) => {
    switch(action.type){
        case 'INCREMENT':
            return {...state, counter: state.counter + 1 }

        case 'DECREMENT':
            return {...state, counter: state.counter - 1 }

        default:
            return state        
    }
}*/


const counterReducer = (state = 0, action) => {
    switch(action.type){
        case 'INCREMENT':
            return state + 1

        case 'DECREMENT':
            return state - 1

        default:
            return state        
    }
}

/*const rootReducer = (state, action) => {
    switch(action.type){
        default:
            return {
                ...state,
                counter: counterReducer(state.counter, action)
                todos: todosReducer(state.todos, action)
            }
    }
}*/
const rootReducer = combineReducers({
    counter: counterReducer,
    counter2: counterReducer
})

window.store = createStore(rootReducer, initialState)


 export class Counter extends React.Component{

    constructor(){
        super()
        this.state = {
            counter: 0,
            counter2: 0
        }
    }

    componentWillMount(){
        store.subscribe( () => {
            this.setState( store.getState() )
        })
    }
    
    render(){
        return <div>
            <div> Counter: {this.state.counter} </div>
            <div> Counter: {this.state.counter2} </div>
            <button onClick={()=> store.dispatch(INCREMENT) }>+</button>
            <button onClick={()=> store.dispatch(DECREMENT) }>-</button>
        </div>
    }
 }