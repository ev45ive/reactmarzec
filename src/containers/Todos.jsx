import React from 'react';
import {TodoForm, ItemsList} from '../components'
import { store, ADD_TODO, REMOVE_TODO} from '../stores/reduxStore.js'

const CheckUpdate = (Component, key) => {
    class CheckUpdate extends React.Component {
        shouldComponentUpdate( newProps={}, newState={} ){
            return this.props[key] != newProps[key]
        }
        render(){
            return <Component {...this.props} />
        }
    }

    return CheckUpdate;
}

const CheckedItemsList = CheckUpdate(ItemsList, 'todos');

const TodoItem = ({item, removeTodo}) => (
    <li className="list-group-item">
        <span> { item.name }</span> 
        <span onClick={e => removeTodo(item)}> &times; </span> 
    </li>
)

export class Todos extends React.Component {

    constructor(){
        super()
        this.state = {}
    }

    componentWillMount(){
        const {todos,newTodo} = store.getState();

        this.setState({
            todos,
            newTodo
        })
        this.unsubscribe =  store.subscribe(()=>{  
            this.setState({
                todos: store.getState().todos
            })
        })
    }

    componentWillUnmount(){
        this.unsubscribe()
    }
    
    removeTodo = (todo) => {
        store.dispatch({
            type: REMOVE_TODO,
            payload: {
                id: todo.id
            }
        })
    }

    addTodo = () => {
        store.dispatch({
            type: ADD_TODO,
            payload: {
                todo: this.state.newTodo
            }
        })

        // var todos = this.state.todos;
        // this.state.newTodo.id = Date.now()
        // todos.push(this.state.newTodo)
        this.setState({
            // todos: [ ...this.state.todos, { 
            //     ...this.state.newTodo, 
            //     id: Date.now() 
            // } ],
            newTodo: {
                name: '', completed:false
            }
        })
    }

    nameChange = (e) => {
        this.setState({
            newTodo: {
                name: e.target.value,
                completed:false
            }
        })
    }

    render(){
        const {todos, newTodo} = this.state;

        return <div>
            <h3>Lista Todo</h3>
            
            <ItemsList items={todos} ListItem={TodoItem}
                removeTodo={this.removeTodo}  />
            
            <TodoForm newTodo={newTodo} 
                      nameChange={this.nameChange}
                      addTodo={this.addTodo}  />
        </div>

    }

}