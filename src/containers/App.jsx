import React from 'react';
import { Todos } from './Todos.jsx'
import { Users } from './Users.jsx'


export class App extends React.Component {

    constructor(){
        super()
        this.state = {
            tab: 'todos'
        }
    }
    
    select(tab){
        this.setState({
            tab
        })
    }

    render(){
        return <div> 
            <nav className="navbar navbar-light bg-faded">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link" onClick={() => this.select('users')}>Users</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" onClick={() => this.select('todos')}>Todos</a>
                    </li>
                </ul>
            </nav>
            {[
                <tab name="todos">
                    <Todos/>
                </tab>, 
                <tab name="users">
                    <Users/>
                </tab>
            ].find( item => item.props.name == this.state.tab ).props.children}
        </div>
    }
}
