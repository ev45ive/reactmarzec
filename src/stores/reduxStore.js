 import React from 'react'
 import { createStore, combineReducers } from 'redux'

// export ACTIONs CONSTANTS:
export const ADD_TODO = "ADD_TODO"
export const REMOVE_TODO = "REMOVE_TODO"


// initialState
const initialState = {
    todos:[
        {id: 1, name: "Zrobić Zakupy!", completed:true},
        {id: 2, name: "Nauczyć się Reacta", completed:false},
    ]
}

// todoReducer:
const todosReducer = (state = [], action) => {
    switch(action.type){
        case ADD_TODO:
            return [...state, {
                ...action.payload.todo,
                id: Date.now()
            }]
        case REMOVE_TODO:
            let index = state.findIndex( todo => todo.id === action.payload.id)
            return [ ...state.slice(0,index), ...state.slice(index+1) ]
        default:
            return state
    }
}

export const LOAD_USERS_PENDING = "LOAD_USERS_PENDING"
export const LOAD_USERS_DONE = "LOAD_USERS_DONE"
export const LOAD_USERS_ERROR = "LOAD_USERS_ERROR"
export const SELECT_USER = "SELECT_USER"

const usersReducer = (state = {
    data: [],
    entities: {}
},action) => {
    switch(action.type){
            
            case SELECT_USER:
                return { 
                    ...state,
                    selected: action.payload.id
                }
            case LOAD_USERS_PENDING:
                return { 
                    ...state, 
                    pending: true, 
                    error: ''
                }
            case LOAD_USERS_DONE:

                const entities = action.payload.data.reduce( (map, item) => {
                    map[item.id] = item;
                    return map;
                }, {})
                const list = action.payload.data.map( item => item.id )


                return { 
                    ...state, 
                    pending: false,
                    entities: entities,
                    list: list,
                    data: action.payload.data
                }
            case LOAD_USERS_ERROR:
                return { 
                    ...state, 
                    pending: false, 
                    error: action.payload.error
                }
            default:
                return state
    }  
}



// root combined reducer:
const rootReducer = combineReducers({
    todos: todosReducer,
    users: usersReducer,
    //costam: combineReducers({ ... })
})

// export store
export const store = createStore(rootReducer,initialState);

store.subscribe( () => console.log('STORE:',store.getState()) )


export const select_user = (user) => {
    
        store.dispatch({
            type: SELECT_USER,
            payload:{
                id: user.id
            }
        })
}

export const load_users = () =>{    

        store.dispatch({
            type: LOAD_USERS_PENDING,
            payload: { users: [] , pending: true }
        })

        fetch('http://localhost:3000/users/')   /* throw */
        .then( resp => resp.ok? resp.json() : Promise.reject('Błąd '+resp.statusText)  )
        .then( data => {
            store.dispatch({
                type: LOAD_USERS_DONE,
                payload: { data, pending: false }
            })
        })
        .catch( error => {
            store.dispatch({
                type: LOAD_USERS_ERROR,
                payload: {
                    status: error.status,
                    error: error.statusText
                }
            })
        })
}