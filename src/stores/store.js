const EventEmitter = require('events');

export class Store  extends EventEmitter{
    CHANGE_EVENT = 'change'

    constructor(state = {}, actionsHandler ){
        super()
        this.state = state
        this.actionsHandler = actionsHandler;
    }

    handleAction(action){
        this.actionsHandler(this.state, action)
        this.notify()
    }

    notify(){
        this.emit(this.CHANGE_EVENT)
    }

    getState(){
        return this.state
    }
    subscribe(listener){
        this.on(this.CHANGE_EVENT, listener)
    }
    unsubscribe(listener){
        this.removeListener(this.CHANGE_EVENT, listener)
    }
}