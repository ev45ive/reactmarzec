import {Store} from './store.js'

export const ADD_TODO = "ADD_TODO"
export const REMOVE_TODO = "REMOVE_TODO"


export const todosStore = new Store({
    todos: [
        {id: 1, name: "Zrobić Zakupy!", completed:true},
        {id: 2, name: "Nauczyć się Reacta", completed:false},
    ],
    newTodo: {
        name: '',
        completed: false
    }
}, (state, action) => {

    switch(action.type){
        case ADD_TODO:{
            state.todos = [ ...state.todos, { 
                 ...action.payload.todo, 
                 id: Date.now() 
            } ]
        }
        break;
        case REMOVE_TODO:{
            // no op...
        }
        break;
        default:{

        }
    }

})