import {Store} from './store.js'

export const LOAD_USERS_PENDING = "LOAD_USERS_PENDING"
export const LOAD_USERS_DONE = "LOAD_USERS_DONE"
export const LOAD_USERS_ERROR = "LOAD_USERS_ERROR"

export const usersStore = new Store({
    users: []
}, (state, action) => {

    switch(action.type){
        case LOAD_USERS_PENDING:{
            state.pending = action.payload.pending
        }
        break;
        case LOAD_USERS_DONE:{
            state.pending = action.payload.pending
            state.users = action.payload.users
        }
        break;
        case LOAD_USERS_ERROR:{
            state.pending = action.payload.pending
            state.error = action.payload.error
        }
        break;
        default:{
        }
    }
})
// action creator
export const load_users = () =>{    

        usersStore.handleAction({
            type: LOAD_USERS_PENDING,
            payload: { users: [] , pending: true }
        })

        fetch('http://localhost:3000/users/')   /* throw */
        .then( resp => resp.ok? resp.json() : Promise.reject('Błąd '+resp.statusText)  )
        .then( users => {
            usersStore.handleAction({
                type: LOAD_USERS_DONE,
                payload: { users, pending: false }
            })
        })
        .catch( error => {
            usersStore.handleAction({
                type: LOAD_USERS_ERROR,
                payload: {
                    status: error.status,
                    error: error.statusText
                }
            })
        })
}