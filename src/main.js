import 'bootstrap/dist/css/bootstrap.css';
import './style.css';

import React from 'react';
import ReactDOM from 'react-dom';

import { App, Users, Todos } from './containers'
import { Layout } from './components'

import { BrowserRouter as Router, Route, Link, Redirect } from 'react-router-dom'

const Test = ()=><div>Test</div>;

//import {Counter} from './containers/counter.jsx'


ReactDOM.render( <div> 
    <Router>
        <Layout title="Witaj w React!">
            {/*<Route path="/" exact component={Counter} />*/}
            <Route path="/" render={()=><Redirect to="/users"/>} />
            <Route path="/users" component={Users} />
            <Route path="/todos" component={Todos} />
        </Layout>  
    </Router>   
</div> , document.getElementById('root') )


