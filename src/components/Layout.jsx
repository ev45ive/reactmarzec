import React from 'react';
import { Link } from 'react-router-dom'

export const Layout = props => (
    <div>
      <nav className="navbar navbar-light bg-faded">
        <div className="container">
        <ul className="navbar-nav">
            <li className="nav-item">
                <Link to="/users" className="nav-link">Users</Link>
            </li>
            <li className="nav-item">
                <Link to="/todos" className="nav-link">Todos</Link>
            </li>
        </ul>
        </div>
    </nav>
    <div className="container">
        <div className="row">
            <div className="col">
                {props.children}
            </div>
        </div>
    </div>
</div>

)