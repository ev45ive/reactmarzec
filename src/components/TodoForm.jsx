import React from 'react';

export const TodoForm = ({
    newTodo = { name: '', completed: false },
    nameChange,
    addTodo,
}) => ( 
<div className="input-group">
    <input
        ref={ i => i && i.focus() }
        onKeyUp={e=> e.key == "Enter" && addTodo()}
        value={newTodo.name} 
        onChange={nameChange} 
        className="form-control" 
        placeholder="dodaj todo..." /> 
    <button onClick={addTodo}>Dodaj</button>
</div> )