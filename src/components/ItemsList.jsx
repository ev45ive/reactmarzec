import React from 'react';

export const  ItemsList = ({items = [], removeTodo, ListItem}) => {
   return items.length? 
        <ul className="list-group">
        {items.map( (item, index) => 
            <ListItem item={item} removeTodo={removeTodo}  key={item.id} />
        )}
        </ul>
    : <div> Brak Elementów. </div>   
}