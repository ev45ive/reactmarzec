import React from 'react';

const styles={
    table:{
        border: '2px solid black'
    }
}

export const Message = ({
    i = 1,
    list = [1,2,3] 
}) => <div id="message">
        <p>Witaj w React!</p>
        {/* (i % 2 ==0) && <img src="https://placekitten.com/640/640" width="150" /> */}
        <table style={styles.table}>
            <tbody>
            { list.map( x => <tr  style={{"background": (x % 2 == 0? "#666" : "")}} key={x}>
                <td>{ x }</td>
            </tr>) }
            </tbody>
        </table>
        <p className={"btn-" + (i % 2 ==0 ? "success":"danger")}>Liczę do : { i } </p>
    </div>