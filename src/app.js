
class Base{
    constructor(){
        this.options = {}
    }
    run(){
        console.error('Please Implement!')
    }
}

class App extends Base{
    constructor(conf){
        super()
        this.conf = conf;
        this.options.DEBUG = true;
    }
    run(){
        console.log('Wersja : ',this.version,this.conf)
    }
    version = '1.1.0'
}

export const CONFIG = {
    something: 123
}

export default App;
//module.exports = App;